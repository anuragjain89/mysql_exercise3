DROP DATABASE IF EXISTS vtapp;
CREATE DATABASE vtapp;
SHOW DATABASES;

GRANT USAGE ON vtapp.* TO 'vtapp_user';
DROP USER 'vtapp_user';

CREATE USER 'vtapp_user' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON vtapp . * TO vtapp_user;
SHOW GRANTS FOR 'vtapp_user';
FLUSH PRIVILEGES;